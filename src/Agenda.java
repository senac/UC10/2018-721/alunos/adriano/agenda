import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Agenda {

    public static void exibirSobre(String mensagem) {
        String sobre = "\nDesenvolvido por: Adriano Barbosa da Trindade\n"
                + "Turma: 721 de 2019\n";
        JOptionPane.showMessageDialog(null, sobre, "Sobre", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void exibirImpressao(List<Contato> list) {
        String impressao = "";
        for (int i = 0; i < list.size(); i++) {
            impressao += ("NOME: " + list.get(i).nome + "\n" + "TELEFONE: " + list.get(i).telefone + "\n\n");
        }

        JOptionPane.showMessageDialog(null, impressao, "Contatos Cadastrados", JOptionPane.INFORMATION_MESSAGE);
    }

    public static void exibirErro(String mensagem) {
        JOptionPane.showMessageDialog(null, mensagem, "Atencao!", JOptionPane.ERROR_MESSAGE);
    }

    public static void exibirExiste(List<Contato> list) {
        int posicao = 0;
        Boolean nomeProcurarexiste = false;
        String nomeProcurar;
        nomeProcurar = JOptionPane.showInputDialog(null, "Entre com um nome para ser pesquisado", "Pesquisar Nome", JOptionPane.QUESTION_MESSAGE);

        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).nome.equalsIgnoreCase(nomeProcurar)) {
                nomeProcurarexiste = true;
                posicao = i;
            }
        }

        if (nomeProcurarexiste == true) {
            JOptionPane.showMessageDialog(null, "O nome: " + nomeProcurar + " existe na agenda, na posicao: [" + posicao + "]", "Resultado", JOptionPane.INFORMATION_MESSAGE);
        } else {
            JOptionPane.showMessageDialog(null, "O nome: " + nomeProcurar + " nao existe na agenda", "Resultado", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static void exibirRemover(List<Contato> list) {
        Boolean nomeRemoverExiste = false;
        String nomeRemover;
        nomeRemover = JOptionPane.showInputDialog(null, "Entre com o nome para ser removido da agenda", "Remover Contato", JOptionPane.QUESTION_MESSAGE);
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).nome.equalsIgnoreCase(nomeRemover)) {
                list.remove(i);
                JOptionPane.showMessageDialog(null, "O contato foi removido da agenda!", "Resultado", JOptionPane.INFORMATION_MESSAGE);
                break;
            } else {
                nomeRemoverExiste = true;
            }
        }

        if (nomeRemoverExiste == true) {
            JOptionPane.showMessageDialog(null, "O contato nao exite na agenda", "Resultado", JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public static class Contato {

        String nome;
        String telefone;
    }

    public static void main(String[] args) {
        int opc;
        String aux;
        String sobre = "";
        List<Contato> agenda = new ArrayList<Contato>();

        do {

            String menu = "                         Contatos (" + agenda.size() + ")\n\n"
                    + "1 - Novo Contato\n"
                    + "2 - Buscar Contato por Nome\n"
                    + "3 - Remover Contato\n"
                    + "4 - Lista Todos os Contatos\n"
                    + "5 - Sobre a Agenda de Contatos\n"
                    + "0 - Sair\n\n";

            aux = JOptionPane.showInputDialog(null, menu, "#####   Agenda de Contatos   #####", JOptionPane.PLAIN_MESSAGE);
            opc = Integer.parseInt(aux);

            switch (opc) {
                case 1:

                    Contato contato = new Contato();

                    String nome;
                    String telefone;

                    nome = JOptionPane.showInputDialog(null, "Entre com o nome do contato", "Cadastrando Nome", JOptionPane.PLAIN_MESSAGE);
                    telefone = JOptionPane.showInputDialog(null, "Entre com o telefone do contato", "Cadastrando Telefone", JOptionPane.PLAIN_MESSAGE);

                    contato.nome = nome;
                    contato.telefone = telefone;
                    agenda.add(contato);

                    //agenda.isEmpty();
                    break;
                case 2:
                    exibirExiste(agenda);
                    break;
                case 3:
                    exibirRemover(agenda);
                    break;
                case 4:
                    exibirImpressao(agenda);
                    break;
                case 5:
                    exibirSobre(sobre);
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    exibirErro("Opcao Invalida");
            }

        } while (true);

    }

}

//https://www.clubedohardware.com.br/forums/topic/1045497-agenda-com-arraylist/